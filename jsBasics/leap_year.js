function isLeapYear(year){
    const rem100=year%100;
    const rem400=year%400;
    const rem4=year%4;
    if(rem400==0 ||(rem100!=0 && rem4==0))
    return true;
    else
    return false;

}
function checkLeapYear(year){
    const leap = new Date(year, 1, 29).getDate() === 29;
    if(leap)
    return true;
    else
    return false;
}
let year=1700;
console.log(isLeapYear(year));
console.log(checkLeapYear(year));