function welcomeGuest(name,greetHandle){
  greetHandle(name);
}
function greetings(name){
    console.log('Hi there,',name);
}
function greetNight(name){
    console.log('go to sleep',name);
}
function greetAfterNoon(name){
    console.log('take tea',name);
}
const actorName= 'Tom Hanks';
welcomeGuest(actorName,greetAfterNoon);
welcomeGuest('Farhan',greetings);
welcomeGuest('Barkat',greetNight);
welcomeGuest('Rady',function(){
console.log('This is boss');
});