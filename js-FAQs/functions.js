function evenOrOdd(num) {
    if (num % 2 == 0) {
        console.log(num + ': is an even number');
    }
    else {
        console.log(num + ': is an odd number');
    }

}
function evenify(num){
    var result;
    if(num%2==0)  result=num;
    else result=num*2;
    return result;
}
var result=evenify(13);
console.log('result: ',result*result);
function getArray(arr) {
    for (let i = 0; i < arr.length; i++) {
        let num = arr[i];
        evenOrOdd(num);
    }
}
const nums = [12, 32, 43, 22, 42, 5];
getArray(nums);

let friendsAge = [32, 43, 12, 11, 34, 24];
getArray(friendsAge);