function feetToMile(feet){
   return feet/5280;
}
function woodCalculator(chair,table,khat){
    return chair+table*3+khat*5;

}
function brickCalculator(tala){
    let brickNeed=0;
    let remaining=0;
    if(tala<=10){
         brickNeed=15*tala*1000;
    }
    else if(tala <=20){
        brickNeed=10*15*1000;
         remaining=(tala-10)*12*1000;
        brickNeed+=remaining;
    }
    else{

        brickNeed=10*15*1000+10*12*1000;
        remaining=(tala-20)*10*1000;
        brickNeed+=remaining;
    }
    return brickNeed;

}
function tinyFriend(arr){
    let minVal=arr[0];
    let smallArr=[];
    for(let i=0;i<arr.length;i++){
       if(arr[i].length<minVal.length) minVal=arr[i];
    }
    let size=minVal.length;
    for(let i=0;i<arr.length;i++){
        if(arr[i].length==minVal.length) smallArr.push(arr[i]);
     }
     return smallArr;

}
//feet to mile answer
const getFeet=12000;
const getMile=feetToMile(getFeet);
console.log(getFeet+'foot= '+getMile+' mile');
//wood calculator answer
const getChair=15;
const getTable=3;
const getKhat=10;
const total= woodCalculator(getChair,getTable,getKhat);
console.log('total wood needed= '+total);
//tinyFriend answer
const arr=['Farhan','Rady','Sayem','Dahy','Murshed'];
const small= tinyFriend(arr);
console.log('Small string found= '+small);
//brickCalculator
const getTalaCount=23;
const brickNeed=brickCalculator(getTalaCount);
console.log('Brick lagbe= '+brickNeed+' ti');
