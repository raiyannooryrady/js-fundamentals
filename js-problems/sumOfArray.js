function sumOfArray(arr) {
    let arrSize = arr.length, sum = 0;
    for (let i = 0; i < arrSize; i++) {
        sum += arr[i];

    }
    return sum;
}
function maxOfArray(arr) {
    let arrSize = arr.length, maxVal = arr[0];
    for (let i = 0; i < arrSize; i++) {
        if (arr[i] > maxVal)
            maxVal = arr[i];

    }
    return maxVal;
}
function minOfArray(arr) {
    let arrSize = arr.length, minVal = arr[0];
    for (let i = 0; i < arrSize; i++) {
        if (arr[i] < minVal)
            minVal = arr[i];

    }
    return minVal;
}
let arr = [22, 34, 42, 532, 43, 34];
console.log('Max value= ' + maxOfArray(arr));
console.log('Min value= ' + minOfArray(arr));
console.log('Sum of Array = ' + sumOfArray(arr));
